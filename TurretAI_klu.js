/** Exporté le 5/9/2023, 9:48:39 AM **/

/** TURRET AI **/

/**
 * Best turret AI program by RastaLucas -w-
**/

/*** GLOBAL VARIABLES ***/
global state;

global lowestAlly;
global fightingAlly;
global closestEnemy;
global turret;



/*** FUNCTIONS ***/

/** Get allies and enemies **/
/* Get lowest ally */
function getLowestAlly() {
	var aliveAllies = getAliveAllies();
	var minLife = 99999;
	var lowest = null;
	for (var ally in aliveAllies) {
		if (getType(ally) == ENTITY_TURRET) continue;
		if (getLife(ally) < minLife && canUseChip(CHIP_CURE, ally)) {
			minLife = getLife(ally);
			lowest = ally;
		}
	}
	return lowest;
}

/* Get fighting ally (closest to an enemy) */
function getFightingAlly() {
	var aliveAllies = getAliveAllies();
	var aliveEnemies = getAliveEnemies();
	var minDist = 99999;
	var fightest = null;
	for (var ally in aliveAllies) {
		if (getType(ally) == ENTITY_TURRET) continue;
		for (var enemy in aliveEnemies) {
			if (getPathLength(getCell(ally), getCell(enemy)) < minDist && canUseChip(CHIP_MOTIVATION, ally)) {
				minDist = getPathLength(getCell(ally), getCell(enemy));
				fightest = ally;
			}
		}
	}
	return fightest;
}

/* Get closest enemy */
function getClosestEnemy() {
	var aliveEnemies = getAliveEnemies();
	var minDist = 99999;
	var closest = null;
	for (var enemy in aliveEnemies) {
		if (getCellDistance(getCell(turret), enemy) < minDist && canUseChip(CHIP_VENOM, enemy)) {
			minDist = getCellDistance(getCell(turret), enemy);
			closest = enemy;
		}
	}
	return closest;
}


/** Chips functions **/
/* Buffing chips */
function buffLeek() {
	if (canUseChip(CHIP_SHIELD, fightingAlly)) useChip(CHIP_SHIELD, fightingAlly);
	if (canUseChip(CHIP_WALL, fightingAlly)) useChip(CHIP_WALL, fightingAlly);
	if (canUseChip(CHIP_MOTIVATION, fightingAlly)) useChip(CHIP_MOTIVATION, fightingAlly);
}

/* Healing/buffing chips */
function healLeek() {
	if (canUseChip(CHIP_CURE, lowestAlly)) useChip(CHIP_CURE, lowestAlly);
	if (canUseChip(CHIP_SHIELD, lowestAlly)) useChip(CHIP_SHIELD, lowestAlly);
	if (canUseChip(CHIP_WALL, lowestAlly)) useChip(CHIP_WALL, lowestAlly);
}

/* Attack chips */
function attackLeek() {
	if (canUseChip(CHIP_VENOM, closestEnemy)) useChip(CHIP_VENOM, closestEnemy);
	if (canUseChip(CHIP_SLOW_DOWN, closestEnemy)) useChip(CHIP_SLOW_DOWN, closestEnemy);
	if (canUseChip(CHIP_SHOCK, closestEnemy)) useChip(CHIP_SHOCK, closestEnemy);
}


/** Miscellaneous functions **/
/* Update global variables */
function update() {
	turret = getEntity();
	lowestAlly = getLowestAlly();
	closestEnemy = getClosestEnemy();
	fightingAlly = getFightingAlly();
	
	if (lowestAlly != null && getLife(lowestAlly) <= getTotalLife(lowestAlly)/2) state = "Healing";
	else if (closestEnemy != null) state = "Attacking";
	else state = "Buffing"
	
	debug(state);
	debug("Lowest ally "+lowestAlly);
	debug("Closest enemy "+closestEnemy);
	debug("Fighting ally "+fightingAlly);
}



/*** SIMULATION STARTS ***/
function main() {
	if (getTurn() == 1) {
		state = "Buffing";
	}
	
	update();
	
	if (state = "Buffing") {
		if (fightingAlly != null) {
			buffLeek();
		}
		if (lowestAlly != null) {
			healLeek();
		}
	}
	
	if (state = "Healing") {
		healLeek();
		if (fightingAlly != null) {
			buffLeek();
		}
	}
	
	if (state = "Attacking") {
		attackLeek();
		if (fightingAlly != null) {
			buffLeek();
		}
	}
}

main();

