/**
 * Bienvenue sur Leek Wars !
 * Pour connaître les règles du jeu : leekwars.com/encyclopedia/fr/Règles_du_jeu
 * Pour apprendre à coder son IA : leekwars.com/encyclopedia/fr/Tutoriel
**/

// Ceci est un code d'exemple très basique :

// On prend le pistolet

// On récupère l'ennemi le plus proche
global enemy = getNearestEnemy();
global ally = findChadLeek();
global turret = getEntity();

function findChadLeek(){
	var currentClosestDistance = 99999;
	var currentClosestAlly;
	var aliveAllies = getAliveAllies();
	for (var entity in aliveAllies) {
		if (getType(entity) == ENTITY_LEEK) {
			var distance = getCellDistance(getCell(getEntity()), getCell(entity));
			if (distance < currentClosestDistance) {
				currentClosestAlly = entity;
				currentClosestDistance = distance;
			}
		}

	}
	return currentClosestAlly;
}
useChipOnce(CHIP_MOTIVATION, ally);
useChipOnce(CHIP_THORN, ally);
useChipOnce(CHIP_WALL, ally);


useChipOnce(CHIP_VENOM, enemy);
useChipOnce(CHIP_SHOCK, enemy);
useChipOnce(CHIP_SLOW_DOWN, enemy);



useChipOnce(CHIP_CURE, ally);


function useChipOnce(chip, target) {
	
	if (canUseChip(chip, target) and getChipCost(chip) <= getTP(turret)) useChip(chip, target);
};
